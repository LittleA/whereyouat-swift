//
//  LoginViewController.swift
//  WhereYouAt
//
//  Created by MuRay Lin on 2016/3/20.
//  Copyright © 2016年 MuRay Lin. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    var spinner: UIActivityIndicatorView?
    var accountLabel: UILabel?
    var accountTextField: UITextField?
    var passwordLabel: UILabel?
    var passwordTextField: UITextField?
    var loginButton: UIButton?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initViewComponents()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func initViewComponents() {
        
        let halfMargin: CGFloat = 8
        let margin: CGFloat = 16
        let doubleMargin: CGFloat = 32
        
        self.view.backgroundColor = UIColor.whiteColor()
        
        // UIActivityIndicatorView
        spinner = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        spinner?.center = self.view.center
        spinner?.hidesWhenStopped = true
        self.view.addSubview(spinner!)
        
        // Account Label
        accountLabel = UILabel(frame: CGRectMake(
            margin,
            CGRectGetHeight(self.view.frame) * 0.2,
            CGRectGetWidth(self.view.frame) - doubleMargin,
            CGRectGetHeight(self.view.frame) * 0.05)
        )
        accountLabel?.font = UIFont.systemFontOfSize(14)
        accountLabel?.text = "account"
        accountLabel?.textColor = UIColor.blackColor()
        accountLabel?.textAlignment = .Left
        self.view.addSubview(accountLabel!)
        
        // Account TextField
        accountTextField = UITextField(frame: CGRectMake(
            margin,
            CGRectGetMaxY(accountLabel!.frame) + halfMargin,
            CGRectGetWidth(self.view.frame) - doubleMargin,
            CGRectGetHeight(self.view.frame) * 0.05)
        )
        accountTextField?.autocorrectionType = .No
        accountTextField?.font = UIFont.systemFontOfSize(14)
        accountTextField?.textColor = UIColor.darkGrayColor()
        accountTextField?.textAlignment = .Left
        accountTextField?.placeholder = " Your account"
        accountTextField?.layer.borderColor = UIColor.lightGrayColor().CGColor
        accountTextField?.layer.borderWidth = 1.0
        accountTextField?.layer.cornerRadius = 3.0
        self.view.addSubview(accountTextField!)
        
        // PasswordLabel Label
        passwordLabel = UILabel(frame: CGRectMake(
            margin,
            CGRectGetMaxY(accountTextField!.frame) + margin,
            CGRectGetWidth(self.view.frame) - doubleMargin,
            CGRectGetHeight(self.view.frame) * 0.05)
        )
        passwordLabel?.font = UIFont.systemFontOfSize(14)
        passwordLabel?.text = "password"
        passwordLabel?.textColor = UIColor.blackColor()
        passwordLabel?.textAlignment = .Left
        self.view.addSubview(passwordLabel!)
        
        // PasswordTextField TextField
        passwordTextField = UITextField(frame: CGRectMake(
            margin,
            CGRectGetMaxY(passwordLabel!.frame) + halfMargin,
            CGRectGetWidth(self.view.frame) - doubleMargin,
            CGRectGetHeight(self.view.frame) * 0.05)
        )
        passwordTextField?.autocorrectionType = .No
        passwordTextField?.font = UIFont.systemFontOfSize(14)
        passwordTextField?.textColor = UIColor.darkGrayColor()
        passwordTextField?.textAlignment = .Left
        passwordTextField?.placeholder = " Your password"
        passwordTextField?.layer.borderColor = UIColor.lightGrayColor().CGColor
        passwordTextField?.layer.borderWidth = 1.0
        passwordTextField?.layer.cornerRadius = 3.0
        passwordTextField?.secureTextEntry = true
        self.view.addSubview(passwordTextField!)
        
        // Login Button
        loginButton = UIButton(frame: CGRectMake(
            margin,
            CGRectGetMaxY(passwordTextField!.frame) + margin,
            CGRectGetWidth(self.view.frame) - doubleMargin,
            CGRectGetHeight(self.view.frame) * 0.1)
        )
        loginButton?.setTitle("Login", forState: .Normal)
        loginButton?.setTitleColor(UIColor.blackColor(), forState: .Normal)
        loginButton?.addTarget(self, action: "login", forControlEvents: .TouchUpInside)
        self.view.addSubview(loginButton!)
    }
    
    // MARK: - Action
    func login() {
        spinner?.startAnimating()
        
        let user_username:String = accountTextField!.text!
        let user_password:String = passwordTextField!.text!
        let headers: Dictionary = ["X-Apple-Device-Id": UIDevice.currentDevice().identifierForVendor!.UUIDString]
        let params: Dictionary = ["user_username": user_username, "user_password": user_password]
        let WYA: WhereYouAt = WhereYouAt()
        
        WYA.User.login(params, headers: headers){ (response) -> Void in
            let success = response["success"] as! Bool
            
            self.spinner?.stopAnimating()
            
            dispatch_async(dispatch_get_main_queue()) {
                if (success){
                    let vc = ProfileViewController()
                    vc.profileData = response
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else {
                    var message = WYA.User.Result["message"] as? String
                    if message == nil {
                        message = WYA.User.Error
                    }
                    // Make a UIAlertController
                    let alert = UIAlertController(title: "WARNING", message: message, preferredStyle: UIAlertControllerStyle.Alert)
                    let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil)
                    alert.addAction(cancelAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                    return
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
