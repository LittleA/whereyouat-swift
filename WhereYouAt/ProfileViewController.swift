//
//  ViewController.swift
//  WhereYouAt
//
//  Created by MuRay Lin on 2016/3/20.
//  Copyright © 2016年 MuRay Lin. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int)
    {
        let newRed = CGFloat(red)/255
        let newGreen = CGFloat(green)/255
        let newBlue = CGFloat(blue)/255
        
        self.init(red: newRed, green: newGreen, blue: newBlue, alpha: 1.0)
    }
}

class ProfileViewController: UIViewController {

    var profileData: NSDictionary?
    var coverImageView: UIImageView?
    var mainimageView: UIImageView?
    var desView: UIView?
    var nameLabel: UILabel?
    var desTextView: UITextView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initViewComponents()
        
//        let temp: NSDictionary = [
//            "zulutime": "2015-04-23T03:28:13Z",
//            "user": [
//                "_id": "546ae4865c3eaaf4a68b4569",
//                "user_username": "crwirz",
//                "user_email_addresses": [
//                    "crwirz@gmail.com"
//                ],
//                "user_first_name": "Christopher",
//                "user_last_name": "Wirz",
//                "user_last_update": [
//                    "sec": 1429759694,
//                    "usec": 26000
//                ],
//                "user_main_image_url": "https://s3.amazonaws.com/whereyouat-images/8a4dde99b66a0bcd1ff8df0048951fef2087a254ae9c9a39a51fd38bb85583b91b27336b109de028486e62e67cc8b1b27495725ebaabdf2f441afb61080e4935.png",
//                "user_description": "Christopher is a full stack developer and systems engineer.",
//                "user_cover_image_url": "https://s3.amazonaws.com/whereyouat-images/4e6fad7b19fd53178ac326b8a36b937acc4156bc3328d6df63cfd3927498a6090d4228232301a8be43fc6bf2aa23ba7d52fba460978dd1d99b89e4a307252c4e.jpg",
//                "user_last_update_zulu": "2015-04-22T20:28:14Z",
//                "user_last_update_utc": "2015-04-22 20:28:14",
//                "user_last_update_standard": "04/22/2015",
//                "user_last_update_ago": "0 seconds ago",
//                "user_last_update_seconds": 1429759694.26,
//                "current_user_can_modify": true,
//                "current_user_can_content_edit": true,
//                "user_display_name": "Christopher Wirz",
//                "user_is_current_user": true
//            ],
//            "message": "crwirz was updated in the database.",
//            "success": true
//        ]
        
        initProfileData(profileData!)
        loadProfileData()
    }

    override func viewWillAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func initProfileData(profile: NSDictionary) {
        profileData = profile
    }
    
    func initViewComponents() {
        self.view.backgroundColor = UIColor(red: 78, green: 111, blue: 51)
        
        // Cover Image
        coverImageView = UIImageView(frame: CGRectMake(
            0,
            0,
            CGRectGetWidth(self.view.frame),
            CGRectGetHeight(self.view.frame) * 0.3)
        )
        coverImageView?.contentMode = .Center
        self.view.addSubview(coverImageView!)
        
        // Profile Image
        mainimageView = UIImageView(frame: CGRectMake(
            0,
            0,
            CGRectGetWidth(self.view.frame) * 0.4,
            CGRectGetWidth(self.view.frame) * 0.4)
        )
        mainimageView?.center = CGPointMake(CGRectGetWidth(self.view.frame) / 2, CGRectGetHeight(coverImageView!.frame))
        mainimageView?.layer.cornerRadius = CGRectGetWidth(mainimageView!.frame) / 2
        mainimageView?.layer.borderWidth = 5.0
        mainimageView?.layer.borderColor = UIColor.whiteColor().CGColor
        mainimageView?.clipsToBounds = true
        mainimageView?.contentMode = .Center
        self.view.addSubview(mainimageView!)
        
        // Description View
        let margin: CGFloat = 12
        let doubleMargin: CGFloat = 24
        
        desView = UIView(frame: CGRectMake(
            margin,
            CGRectGetMaxY(mainimageView!.frame) + margin,
            CGRectGetWidth(self.view.frame) - doubleMargin,
            CGRectGetHeight(self.view.frame) - CGRectGetMaxY(mainimageView!.frame) - doubleMargin)
        )
        desView?.backgroundColor = UIColor.whiteColor()
        desView?.layer.borderWidth = 3.0
        desView?.layer.borderColor = UIColor.grayColor().CGColor
        desView?.layer.cornerRadius = 3.0
        desView?.clipsToBounds = true
        self.view.addSubview(desView!)
        
        // Name Label
        nameLabel = UILabel(frame: CGRectMake(
            margin,
            margin,
            CGRectGetWidth(desView!.frame) - doubleMargin,
            CGRectGetHeight(desView!.frame) * 0.1)
        )
        nameLabel?.font = UIFont.systemFontOfSize(26)
        nameLabel?.textColor = UIColor.grayColor()
        nameLabel?.textAlignment = .Center
        desView?.addSubview(nameLabel!)
        
        // Description TextView
        desTextView = UITextView(frame: CGRectMake(
            margin,
            CGRectGetMaxY(nameLabel!.frame),
            CGRectGetWidth(desView!.frame) - doubleMargin,
            CGRectGetMaxY(desView!.frame) - margin)
        )
        desTextView?.font = UIFont.systemFontOfSize(18)
        desTextView?.textColor = UIColor.blackColor()
        desTextView?.textAlignment = .Left
        desTextView?.editable = false
        desView?.addSubview(desTextView!)
    }
    
    func setName(profileName: String) {
        nameLabel?.text = profileName
    }
    
    func setDescription(profileDescription: String) {
        desTextView?.text = profileDescription
    }
    
    func setCoverImage(profileCover: String) {
        let imgURL: NSURL = NSURL(string: profileCover)!
        let task = NSURLSession.sharedSession().dataTaskWithURL(imgURL) {
            (responseData, responseUrl, error) in
            
            if let data = responseData {
                dispatch_async(dispatch_get_main_queue(), {
                    self.coverImageView?.image = UIImage(data: data)
                })
            }
        }
        task.resume()
    }
    
    func setMainImage(profileMain: String) {
        let imgURL: NSURL = NSURL(string: profileMain)!
        let task = NSURLSession.sharedSession().dataTaskWithURL(imgURL) {
            (responseData, responseUrl, error) in
            
            if let data = responseData {
                dispatch_async(dispatch_get_main_queue(), {
                    self.mainimageView?.image = UIImage(data: data)
                })
            }
        }
        task.resume()
    }
    
    func loadProfileData() {
        // define the parameters
//        var params: Dictionary = ["user_username": "crwirz"]
        
        //
        // call the API using the SDK
        // a good endpoint would be WYA.User.getCurrentUser(params)
        //
        
        let desiredAPIResults: NSDictionary = profileData!
        
        let success: Bool = desiredAPIResults["success"] as! Bool!
        if (success) { // will enter the loop only if success evaluates to boolean true
            
            // first, try to see if result will cast as a dictionary, using the optional type
            let userTest: NSDictionary? = desiredAPIResults["user"] as! NSDictionary!
            if userTest != nil {
                
                // knowing it will cast as a dictionary, we cast it again as a traversable dictionary (the optional type is not traversable)
                let user: NSDictionary = desiredAPIResults["user"] as! NSDictionary!
                //
                // this is where the front-end magic happens
                //
                // commands such as println("user=\(user)") will help see what is going on
                //
                // below is the 100% safest way to handle the optional parameters
                for (key, value) in user{
                    // this is the absolute safest way to check for a parameter
                    if key as! String == "user_display_name" {
                        setName(value as! String)
                    }
                    
                    if key as! String == "user_description" {
                        setDescription(value as! String)
                    }
                    
                    if key as! String == "user_cover_image_url" {
                        setCoverImage(value as! String)
                    }
                    
                    if key as! String == "user_main_image_url" {
                        setMainImage(value as! String)
                    }
                }
            }
        }
    }
}

